import React from 'react';
import './App.css';

import Routers from './router';

const App = () => (
	<div className="App">
		<Routers />
	</div>
);

export default App;
