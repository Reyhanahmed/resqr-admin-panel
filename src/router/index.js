import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Auth from '../scenes/Auth';
import Dashboard from '../scenes/Dashboard';

const Routers = () => (
	<BrowserRouter>
		<Switch>
			<Route exact path="/auth" component={Auth} />
			<Route path="/" component={Dashboard} />
		</Switch>
	</BrowserRouter>
);

export default Routers;
