import React, { Component } from 'react';
import { Layout, Menu, Icon } from 'antd';
import { Switch, Route, Link } from 'react-router-dom';

import Home from '../../components/Home';
import Meals from '../../components/Meals';

import './Dashboard.css';

const { Content, Sider } = Layout;

class Dashboard extends Component {
  componentDidMount() {}
  render() {
    const { meQuery } = this.props;
    return (
			<Layout>
				<Sider className="fix-sider">
					<h1 className="logo">RESQR</h1>
					<Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
						<Menu.Item key="1">
							<Link to="/">
								<Icon type="home" />
								<span className="nav-text">Home</span>
							</Link>
						</Menu.Item>
						<Menu.Item key="2">
							<Link to="/meals">
								<Icon type="bars" />
								<span className="nav-text">Meals</span>
							</Link>
						</Menu.Item>
						<Menu.Item key="3">
							<Link to="/tables">
								<Icon type="bars" />
								<span className="nav-text">Tables</span>
							</Link>
						</Menu.Item>
					</Menu>
				</Sider>
				<Layout style={{ marginLeft: 200 }}>
					<Content className="dashboard-content">
						<div className="content-div">
							<Switch>
								<Route exact path="/" render={props => <Home {...props} meQuery={meQuery} />} />
								<Route exact path="/meals" component={Meals} />
							</Switch>
						</div>
					</Content>
				</Layout>
			</Layout>
    );
  }
}

export default Dashboard;
