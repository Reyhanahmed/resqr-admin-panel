import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import Dashboard from './Dashboard';

export const ME_QUERY = gql`
	query MeQuery {
		me {
			id
			name
			email
			address
		}
	}
`;

export default graphql(ME_QUERY, { name: 'meQuery' })(Dashboard);
