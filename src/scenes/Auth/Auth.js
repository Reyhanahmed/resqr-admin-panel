import React from 'react';
import { Tabs, Icon } from 'antd';

import Login from '../../components/Login';
import Signup from '../../components/Signup';
import './Auth.css';

const { TabPane } = Tabs;

const Auth = ({ history }) => (
		<div className="auth-form-container">
			<div className="auth-form-subcontainer">
				<h1>RESQR</h1>
				<Tabs defaultActiveKey="1" className="auth-tabs">
					<TabPane
  tab={
							<span>
								<Icon type="login" />Login
							</span>
						}
  key="1"
					>
						<Login history={history} />
					</TabPane>
					<TabPane
  tab={
							<span>
								<Icon type="user-add" />Signup
							</span>
						}
  key="2"
					>
						<Signup history={history} />
					</TabPane>
				</Tabs>
			</div>
		</div>
);

export default Auth;
