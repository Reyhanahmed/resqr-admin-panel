import React from 'react';
import { Form, Icon, Input, Button, Row, Col, message } from 'antd';

import './Signup.css';

const FormItem = Form.Item;

class NormalSignupForm extends React.Component {
	handleSubmit = (e) => {
	  e.preventDefault();
	  this.props.form.validateFields(async (err, values) => {
	    const { registerRestaurantMutation, history } = this.props;
	    if (!err) {
	      const { email, password } = values;
	      const res = await registerRestaurantMutation({
	        variables: { email, password },
	      });
	      const {
	        ok, token, refreshToken, errors,
	      } = res.data.registerRestaurant;
	      if (ok) {
	        localStorage.setItem('resqr-token', token);
	        localStorage.setItem('resqr-refresh-token', refreshToken);
	        history.push('/');
	      } else {
	        message.error(errors[0].message);
	      }
	    }
	  });
	};

	checkPassword = (rule, value, callback) => {
	  const { form } = this.props;
	  if (value && value !== form.getFieldValue('password')) {
	    callback('Passwords do not match!');
	  } else {
	    callback();
	  }
	};

	render() {
	  const { getFieldDecorator } = this.props.form;
	  return (
			<Form onSubmit={this.handleSubmit}>
				<Row>
					<Col span={18} offset={3}>
						<FormItem>
							{getFieldDecorator('email', {
								rules: [
									{ type: 'email', message: 'The input is not a valid E-mail.' },
									{ required: true, message: 'Please input your email!' },
								],
							})(<Input
  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
  placeholder="Username"
							/>)}
						</FormItem>
					</Col>
					<Col span={18} offset={3}>
						<FormItem>
							{getFieldDecorator('password', {
								rules: [{ required: true, message: 'Please input your Password!' }],
							})(<Input
  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
  type="password"
  placeholder="Password"
							/>)}
						</FormItem>
					</Col>
					<Col span={18} offset={3}>
						<FormItem>
							{getFieldDecorator('confirmPassword', {
								rules: [
									{ required: true, message: 'Please input your Password!' },
									{ validator: this.checkPassword },
								],
							})(<Input
  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
  type="password"
  placeholder="Confirm Password"
							/>)}
						</FormItem>
					</Col>
					<Col span={18} offset={3}>
						<FormItem className="btn-form-item">
							<Button type="primary" size="large" htmlType="submit" className="login-form-button">
								Signup
							</Button>
						</FormItem>
					</Col>
				</Row>
			</Form>
	  );
	}
}

const Signup = Form.create()(NormalSignupForm);

export default Signup;
