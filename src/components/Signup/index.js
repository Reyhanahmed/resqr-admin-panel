import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import Signup from './Signup';

const REGISTER_RESTAURANT_MUTATION = gql`
	mutation RegisterRestaurantMutation($email: String!, $password: String!) {
		registerRestaurant(email: $email, password: $password) {
			ok
			token
			refreshToken
			errors {
				path
				message
			}
		}
	}
`;

export default graphql(REGISTER_RESTAURANT_MUTATION, { name: 'registerRestaurantMutation' })(Signup);
