import React from 'react';
import { Modal, Form, Input } from 'antd';

const FormItem = Form.Item;

const InfoModal = Form.create()((props) => {
  const {
    visible, onCancel, onCreate, form,
  } = props;
  const { getFieldDecorator } = form;
  return (
		<Modal
  visible={visible}
  title="Update Restaurant Information"
  okText="Update"
  onCancel={onCancel}
  onOk={onCreate}
		>
			<Form layout="vertical">
				<FormItem label="Name">
					{getFieldDecorator('name', {
						rules: [{ required: true, message: 'Please input the name of restaurant!' }],
					})(<Input />)}
				</FormItem>
				<FormItem label="Address">
					{getFieldDecorator('address', {
						rules: [{ required: true, message: 'Please input the address of restaurant!' }],
					})(<Input type="textarea" />)}
				</FormItem>
			</Form>
		</Modal>
  );
});

export default InfoModal;
