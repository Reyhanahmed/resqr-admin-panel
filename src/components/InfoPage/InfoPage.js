import React, { Component } from 'react';
import { Spin } from 'antd';

import UploadResPics from '../UploadResPics';
import './InfoPage.css';
import MAP_API_KEY from '../../api-key';

class InfoPage extends Component {
	state = {
	  fetching: true,
	  position: {},
	};
	componentDidMount() {
	  const { address } = this.props;
	  this.setState({ fetching: true });
	  fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${MAP_API_KEY}`)
	    .then(res => res.json())
	    .then((res) => {
	      this.setState({ fetching: false, position: res.results[0].geometry.location });
	    })
	    .catch(err => console.log(err.message));
	}
	render() {
	  const { name, address, images: { ok, images } } = this.props;
	  let newImages = {};
	  if (ok) {
	    newImages = images.map((image) => {
	      const { id, ...rest } = image;
	      return { uid: id, ...rest };
	    });
	  }
	  const { fetching, position: { lat, lng } } = this.state;
	  return (
			<div className="info-page-container">
				{fetching ? (
					<div className="info-page-spin">
						<Spin size="large" />
					</div>
				) : (
					<div className="info-content">
						<h1 className="head-name">{name}</h1>
						<p className="rest-address">{address}</p>
						<img
  className="map-image"
  alt=""
  src={`https://maps.googleapis.com/maps/api/staticmap?markers=color:red%7Csize:mid%7C${lat},${lng}&center=${lat},${lng}&size=700x300&scale=2&zoom=18&key=${MAP_API_KEY}`}
						/>
						{ok ? (
							<div className="upload-res-pics">
								<UploadResPics images={newImages} />
							</div>
						) : (
							<p>Sorry, there was some error. Could not show uploaded pictures.</p>
						)}
					</div>
				)}
			</div>
	  );
	}
}

export default InfoPage;
