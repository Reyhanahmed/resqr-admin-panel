import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import UploadResPics from './UploadResPics';

const UPLOAD_RES_IMAGE_MUTATION = gql`
	mutation UploadResImage($file: Upload!) {
		uploadResImage(file: $file) {
			id
			url
			type
		}
	}
`;

const DELETE_RES_IMAGE_MUTATION = gql`
	mutation DeleteResImage($id: Int!, $url: String!) {
		deleteResImage(id: $id, url: $url)
	}
`;

export default compose(
  graphql(UPLOAD_RES_IMAGE_MUTATION, { name: 'uploadResImageMutation' }),
  graphql(DELETE_RES_IMAGE_MUTATION, { name: 'deleteResImageMutation' }),
)(UploadResPics);
