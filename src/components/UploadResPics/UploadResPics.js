import React, { Component } from 'react';
import { Upload, Icon, Modal } from 'antd';

import './UploadResPics.css';
import { GET_RES_IMAGES_QUERY } from '../Home';

class UploadResPics extends Component {
	state = {
	  previewVisible: false,
	  previewImage: '',
	  fileList: this.props.images,
	};

	componentWillReceiveProps(nextProps) {
	  this.setState({ fileList: nextProps.images });
	}

	handleCancel = () => this.setState({ previewVisible: false });

	handlePreview = (file) => {
	  this.setState({
	    previewImage: file.url || file.thumbUrl,
	    previewVisible: true,
	  });
	};

	uploadData = async (file) => {
	  const { uploadResImageMutation } = this.props;
	  await uploadResImageMutation({
	    variables: { file },
	    update: (store, { data: { uploadResImage } }) => {
	      const data = store.readQuery({
	        query: GET_RES_IMAGES_QUERY,
	      });
	      data.getResImages.images.push(uploadResImage);
	      store.writeQuery({ query: GET_RES_IMAGES_QUERY, data });
	    },
	    refetchQueries: [{ query: GET_RES_IMAGES_QUERY }],
	  });
	};

	removeImage = async (file) => {
	  const { deleteResImageMutation } = this.props;
	  await deleteResImageMutation({
	    variables: { id: file.uid, url: file.url },
	    update: (store, { data: deleteResImage }) => {
	      if (deleteResImage) {
	        const data = store.readQuery({
	          query: GET_RES_IMAGES_QUERY,
	        });
	        const newImages = data.getResImages.images.filter(image => image.id !== file.uid);
	        data.getResImages.images = newImages;
	        store.writeQuery({ query: GET_RES_IMAGES_QUERY, data });
	      }
	    },
	  });
	};

	render() {
	  const { previewVisible, previewImage, fileList } = this.state;
	  const uploadButton = (
			<div>
				<Icon type="plus" />
				<div className="ant-upload-text">Upload</div>
			</div>
	  );
	  return (
			<div className="clearfix">
				<Upload
  action="//localhost:8081/files/"
  data={this.uploadData}
  listType="picture-card"
  fileList={fileList}
  onPreview={this.handlePreview}
  onRemove={this.removeImage}
  className="upload-con"
				>
					{fileList.length >= 5 ? null : uploadButton}
				</Upload>
				<Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
					<img alt="example" style={{ width: '100%' }} src={previewImage} />
				</Modal>
			</div>
	  );
	}
}

export default UploadResPics;
