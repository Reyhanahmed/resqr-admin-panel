import React, { Component } from 'react';
import { Spin, message } from 'antd';

import Welcome from '../Welcome';
import InfoPage from '../InfoPage';
import './Home.css';
import { ME_QUERY } from '../../scenes/Dashboard';

const Display = ({
  me, visible, handleCreate, handleCancel, saveFormRef, showModal, images,
}) => {
  const { name, address } = me;
  if (!name || !address) {
    return (
			<Welcome
  visible={visible}
  handleCreate={handleCreate}
  handleCancel={handleCancel}
  saveFormRef={saveFormRef}
  showModal={showModal}
			/>
    );
  }
  return <InfoPage name={name} address={address} images={images} />;
};

class Home extends Component {
	state = {
	  visible: false,
	};

	showModal = () => {
	  this.setState({ visible: true });
	};

	handleCancel = () => {
	  this.form.resetFields();
	  this.setState({ visible: false });
	};

	handleCreate = () => {
	  const { form } = this;
	  const { updateResInfoMutation } = this.props;
	  form.validateFields(async (err, values) => {
	    if (!err) {
	      const { name, address } = values;
	      try {
	        await updateResInfoMutation({
	          variables: { name, address },
	          update: (store, { data: { updateResInfo } }) => {
	            const { ok, restaurant } = updateResInfo;
	            if (!ok) {
	              return;
	            }

	            const data = store.readQuery({
	              query: ME_QUERY,
	            });
	            data.me.name = restaurant.name;
	            data.me.address = restaurant.address;
	            store.writeQuery({ query: ME_QUERY, data });
	          },
	        });
	        form.resetFields();
	        this.setState({ visible: false });
	      } catch (error) {
	        message.error(error);
	      }
	    }
	  });
	};
	saveFormRef = (form) => {
	  this.form = form;
	};

	render() {
	  const {
	    meQuery: { loading, me },
	    getResImagesQuery: { loading: imageLoading, getResImages },
	  } = this.props;
	  return (
			<div>
				{loading || imageLoading ? (
					<Spin size="large" />
				) : (
					<Display
  me={me}
  visible={this.state.visible}
  handleCreate={this.handleCreate}
  handleCancel={this.handleCancel}
  saveFormRef={this.saveFormRef}
  showModal={this.showModal}
  images={getResImages}
					/>
				)}
			</div>
	  );
	}
}

export default Home;
