import { graphql, compose } from 'react-apollo';
import gql from 'graphql-tag';

import Home from './Home';

const UPDATE_RES_INFO_MUTATION = gql`
	mutation UpdateResInfoMutation($name: String!, $address: String!) {
		updateResInfo(name: $name, address: $address) {
			ok
			restaurant {
				id
				name
				email
				address
			}
			errors {
				path
				message
			}
		}
	}
`;

export const GET_RES_IMAGES_QUERY = gql`
	query GetResImagesQuery {
		getResImages {
			ok
			images {
				id
				url
				type
			}
			errors {
				path
				message
			}
		}
	}
`;

export default compose(
  graphql(UPDATE_RES_INFO_MUTATION, { name: 'updateResInfoMutation' }),
  graphql(GET_RES_IMAGES_QUERY, { name: 'getResImagesQuery' }),
)(Home);
