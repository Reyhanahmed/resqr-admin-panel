import React, { Component } from 'react';
import { Button } from 'antd';

import InfoModal from '../InfoModal';
import './Welcome.css';

class Welcome extends Component {
  render() {
    const {
      visible, saveFormRef, handleCancel, handleCreate, showModal,
    } = this.props;
    return (
			<div className="welcome">
				<h1 className="info-heading">Welcome!</h1>
				<p className="info-paragraph">
					Sorry to bother you but you must provide some basic information to proceed forward.
					<br />Please click the button below to update your information.
				</p>
				<Button type="primary" onClick={showModal}>
					Update Profile
				</Button>
				<InfoModal
  ref={saveFormRef}
  visible={visible}
  onCancel={handleCancel}
  onCreate={handleCreate}
				/>
			</div>
    );
  }
}

export default Welcome;
