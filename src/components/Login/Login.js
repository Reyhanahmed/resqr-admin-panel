import React from 'react';
import { Form, Icon, Input, Button, Row, Col, message } from 'antd';

import './Login.css';

const FormItem = Form.Item;

class NormalLoginForm extends React.Component {
	handleSubmit = (e) => {
	  e.preventDefault();
	  const { loginRestaurantMutation, history } = this.props;
	  this.props.form.validateFields(async (err, values) => {
	    if (!err) {
	      const { email, password } = values;
	      const res = await loginRestaurantMutation({
	        variables: { email, password },
	      });
	      const {
	        ok, token, refreshToken, errors,
	      } = res.data.loginRestaurant;
	      if (ok) {
	        localStorage.setItem('resqr-token', token);
	        localStorage.setItem('resqr-refresh-token', refreshToken);
	        history.push('/');
	      } else {
	        message.error(errors[0].message);
	      }
	    }
	  });
	};
	render() {
	  const { getFieldDecorator } = this.props.form;
	  return (
			<Form onSubmit={this.handleSubmit} className="login-form">
				<Row>
					<Col span={18} offset={3}>
						<FormItem>
							{getFieldDecorator('email', {
								rules: [
									{ type: 'email', message: 'The input is not valid E-mail.' },
									{ required: true, message: 'Please input your email!' },
								],
							})(<Input
  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
  placeholder="Email"
							/>)}
						</FormItem>
					</Col>
					<Col span={18} offset={3}>
						<FormItem>
							{getFieldDecorator('password', {
								rules: [{ required: true, message: 'Please input your Password!' }],
							})(<Input
  prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
  type="password"
  placeholder="Password"
							/>)}
						</FormItem>
					</Col>
					<Col span={18} offset={3}>
						<FormItem className="btn-form-item">
							<Button type="primary" size="large" htmlType="submit" className="login-form-button">
								Log in
							</Button>
						</FormItem>
					</Col>
				</Row>
			</Form>
	  );
	}
}

const Login = Form.create()(NormalLoginForm);

export default Login;
