import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import Login from './Login';

const LOGIN_RESTAURANT_MUTATION = gql`
	mutation LoginRestaurantMutation($email: String!, $password: String!) {
		loginRestaurant(email: $email, password: $password) {
			ok
			token
			refreshToken
			errors {
				path
				message
			}
		}
	}
`;

export default graphql(LOGIN_RESTAURANT_MUTATION, { name: 'loginRestaurantMutation' })(Login);
